import React, { useState, useCallback, useRef } from 'react';
import produce from 'immer';

const numRows = 30;
const numCols = 40;
const PERIOD = 50; // in ms

const operations = [
  [-1, 1],
  [0, 1],
  [1, 1],
  [-1, 0],
  [1, 0],
  [-1, -1],
  [0, -1],
  [1, -1],
];

const cellSize = 20;

// const rainbow = ['#FF0000', '#FF7F00', ' #FFFF00', '#00FF00', '#0000FF', '#4B0082', '#9400D3'];
const rainbow = ['#F60000', '#FF8C00', ' #FFEE00', '#4DE94C', '#3783FF', '#4815AA'];

const randTo = (to) => () => Math.floor(Math.random() * to);
const randColor = randTo(rainbow.length);
// const getRandColor = () => rainbow[randColor()];
const getRandColor = () => 'red';

// const getCellValue = (cell) => cell.value;

const generateEmptyGrid = () => {
  const grid = [];
  for (let i = 0; i < numRows; i++) {
    // grid.push(Array.from(Array(numCols), () => 0));
    grid.push(Array.from(Array(numCols), () => ({
      value: 0,
      color: getRandColor(),
    })));
  }
  return grid;
};

const generateRandomGrid = () => {
  const grid = [];
  for (let i = 0; i < numRows; i++) {
    grid.push(Array.from(Array(numCols), () => ({
      value: Math.random() > 0.7 ? 1 : 0,
      color: getRandColor(),
    })));
  }
  return grid;
};

const Home = () => {
  const [grid, setGrid] = useState(() => generateEmptyGrid());
  const [running, setRunning] = useState(false);
  const [iterations, setiterations] = useState(0);
  const runningRef = useRef(running);
  runningRef.current = running;

  const runSimulation = useCallback(
    () => {
      // console.log('Running in runsimulation', running);
      if (!runningRef.current) {
        return;
      }
      setGrid((g) => {
        return produce(g, (gridCopy) => {
          g.forEach((row, rowIndex) => {
            row.forEach((col, colIndex) => {
              const neighbours = operations.reduce((acc, [xOffset, yOffset]) => {
                let newRowIndex = rowIndex + yOffset;
                let newColIndex = colIndex + xOffset;

                // If current cell is on first row, cell "above" is the last row (stitched)
                if (newRowIndex < 0) {
                  newRowIndex = numRows - 1;
                // If current cell is in last row, then cell "below" is the first row
                } else if (newRowIndex > numRows - 1) {
                  newRowIndex = 0;
                }

                // If current cell is on first row, then left cell is the last row
                if (newColIndex < 0) {
                  newColIndex = numCols - 1;
                // If current cell is on last row, then right cell is in the first row
                } else if (newColIndex > numCols - 1) {
                  newColIndex = 0;
                }

                acc += g[newRowIndex][newColIndex].value;
                return acc;
              }, 0);
              if (g[rowIndex][colIndex].value === 0 && neighbours === 3) {
                gridCopy[rowIndex][colIndex].value = 1;
              }
              if (neighbours < 2 || neighbours > 3) {
                gridCopy[rowIndex][colIndex].value = 0;
              }
            });
          });
        });
      });
      setiterations((i) => i + 1);
      setTimeout(runSimulation, PERIOD);
    },
    [],
  );

  return (
    <>
      <h1>Game of life</h1>
      <button
        type="button"
        onClick={() => {
          setRunning(!running);
          if (!running) {
            runningRef.current = true;
            runSimulation();
          }
        }}
      >
        {running ? 'stop' : 'run'}
        {' '}
        simulation
      </button>
      <button
        type="button"
        onClick={() => {
          setRunning(false);
          setGrid(generateEmptyGrid());
          setiterations(0);
        }}
      >
        Reset
      </button>
      <button
        type="button"
        onClick={() => {
          setGrid(generateRandomGrid());
        }}
      >
        Random
      </button>
      <span>
        {' '}
        {iterations}
        {' '}
        iterations
      </span>
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: `repeat(${numCols}, ${cellSize}px)`
        }}
      >
        {
          grid.map((row, i) => {
            return row.map((col, k) => {
              // const color = getRandColor();
              return (
                <div
                  key={`${i}-${k}`}
                  onClick={() => {
                    const newGrid = produce(grid, (gridCopy) => {
                      gridCopy[i][k].value = grid[i][k].value ? 0 : 1;
                    });
                    setGrid(newGrid);
                  }}
                  className="gridCell"
                  style={{
                    // width: '20px',
                    // height: '20px',
                    // border: '1px solid black',
                    backgroundColor: grid[i][k].value ? grid[i][k].color : undefined,
                  }}
                />
              );
            });
          })
        }
        <style jsx>
          {`
              .gridCell {
                width: ${cellSize}px;
                height: ${cellSize}px;
                border: 1px solid gray;
              }
            `}
        </style>
      </div>
    </>
  );
};

export default Home;
